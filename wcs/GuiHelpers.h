#ifndef _GUIHELPERS_H
#define _GUIHELPERS_H

#include <Windows.h>
#include <stdio.h>
#include <string>
#include <CommCtrl.h>
#include "resource.h"

enum RoomInformation
{
	USERCOUNT,
	CHANNELNAME,
	TOPIC,
	DOWNLOAD,
	UPLOAD
};

enum PrimaryStatus
{
	NOTUSED,
	CONNECTING,
	HANDSHAKE,
	CONNECTED,
	DISCONNECTED
};

enum ListViewEdits
{
	NAME,
	VERSION,
	IP,
	HOSTNAME,
	FILECOUNT,
	CONNECTION,
	ROOMNAME,
	LASTTEXT,
	ACCESS
};

using namespace std;

class GuiHelpers
{
public:
	static void UpdateInformation(HWND, RoomInformation, wstring);
	static void UpdatePrimary(HWND, int, PrimaryStatus, wstring);
	static void AddUser(HWND, wstring, wstring, wstring, wstring, wstring, wstring, wstring, wstring, wstring);
	static void EditUser(HWND, ListViewEdits, wstring, wstring);
	static void WriteLog(HWND, wstring);
};

#endif