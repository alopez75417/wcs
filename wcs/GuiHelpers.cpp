#include "GuiHelpers.h"

void GuiHelpers::UpdateInformation(HWND hWnd, RoomInformation info, wstring arg)
{
	switch (info)
	{
	case USERCOUNT:
		SendMessage(GetDlgItem(hWnd, STATIC_UCOUNT), WM_SETTEXT, NULL, (LPARAM)((wstring)(L"User Count: " + arg)).c_str());
		break;
	case CHANNELNAME:
		SendMessage(GetDlgItem(hWnd, STATIC_CNAME), WM_SETTEXT, NULL, (LPARAM)((wstring)(L"Channel Name: " + arg)).c_str());
		break;
	case TOPIC:
		SendMessage(GetDlgItem(hWnd, STATIC_TOPIC), WM_SETTEXT, NULL, (LPARAM)((wstring)(L"Topic: " + arg)).c_str());
		break;
	case DOWNLOAD:
		SendMessage(GetDlgItem(hWnd, STATIC_DLRATE), WM_SETTEXT, NULL, (LPARAM)((wstring)(L"Download: " + arg + L"Kbps (" + to_wstring((_wtoi(arg.c_str()) / 1000)) + L"KB/s)")).c_str());
		break;
	case UPLOAD:
		SendMessage(GetDlgItem(hWnd, STATIC_ULRATE), WM_SETTEXT, NULL, (LPARAM)((wstring)(L"Upload: " + arg + L"Kbps (" + to_wstring((_wtoi(arg.c_str()) / 1000)) + L"KB/s)")).c_str());
		break;
	default:
		break;
	}
}

void GuiHelpers::UpdatePrimary(HWND hWnd, int index, PrimaryStatus status, wstring ip)
{
	HWND Primary = NULL;

	switch (index)
	{
	case 0:
		Primary = GetDlgItem(hWnd, STATIC_PRIME1);
		break;
	case 1:
		Primary = GetDlgItem(hWnd, STATIC_PRIME2);
		break;
	case 2:
		Primary = GetDlgItem(hWnd, STATIC_PRIME3);
		break;
	case 3:
		Primary = GetDlgItem(hWnd, STATIC_PRIME4);
		break;
	case 4:
		Primary = GetDlgItem(hWnd, STATIC_PRIME5);
		break;
	case 5:
		Primary = GetDlgItem(hWnd, STATIC_PRIME6);
		break;
	default:
		break;
	}

	switch (status)
	{
	case NOTUSED:
		SendMessage(Primary, WM_SETTEXT, NULL, (LPARAM)((wstring)(L"Disconnected")).c_str());
		break;
	case CONNECTING:
		SendMessage(Primary, WM_SETTEXT, NULL, (LPARAM)((wstring)(L"  Connecting " + ip)).c_str());	
		break;
	case HANDSHAKE:
		SendMessage(Primary, WM_SETTEXT, NULL, (LPARAM)((wstring)(L"  Handshake " + ip)).c_str());
		break;
	case CONNECTED:
		SendMessage(Primary, WM_SETTEXT, NULL, (LPARAM)((wstring)(L"  Connected " + ip)).c_str());
		break;
	case DISCONNECTED:
		SendMessage(Primary, WM_SETTEXT, NULL, (LPARAM)((wstring)L"  Disconnected ").c_str());	
		break;
	default:
		break;
	}
}

void GuiHelpers::AddUser(HWND hWnd, wstring name, wstring version, wstring ip, wstring hostName, wstring fileCount, wstring connection, wstring roomName, wstring lastText, wstring access)
{
	HWND listView = GetDlgItem(hWnd, LIST_USERS);
	LVITEM lvI;
	int itemRow = ListView_GetItemCount(listView);

	lvI.state = 0;
	lvI.stateMask = 0;

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 0;
	lvI.pszText = (wchar_t*)name.c_str();

	ListView_InsertItem(listView, &lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 1;
	lvI.pszText = (wchar_t*)version.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 2;
	lvI.pszText = (wchar_t*)ip.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 3;
	lvI.pszText = (wchar_t*)hostName.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 4;
	lvI.pszText = (wchar_t*)fileCount.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 5;
	lvI.pszText = (wchar_t*)connection.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 6;
	lvI.pszText = (wchar_t*)roomName.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 7;
	lvI.pszText = (wchar_t*)lastText.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.iSubItem = 8;
	lvI.pszText = (wchar_t*)access.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);

	ListView_SetTextBkColor(listView, RGB(0, 0, 0));
	ListView_SetTextColor(listView, RGB(255, 255, 255));
}

void GuiHelpers::EditUser(HWND hWnd, ListViewEdits edit, wstring username, wstring arg)
{
	HWND listView = GetDlgItem(hWnd, LIST_USERS);

	LVFINDINFO search;
	search.flags = LVFI_STRING;
	search.psz = username.c_str();

	int item = ListView_FindItem(listView, -1, &search);

	LVITEM lvI;
	int itemRow = item;
	lvI.state = 0;
	lvI.stateMask = 0;

	switch (edit)
	{
	case NAME:
		lvI.iSubItem = 0;
		break;
	case VERSION:
		lvI.iSubItem = 1;
		break;
	case IP:
		lvI.iSubItem = 2;
		break;
	case HOSTNAME:
		lvI.iSubItem = 3;
		break;
	case FILECOUNT:
		lvI.iSubItem = 4;
		break;
	case CONNECTION:
		lvI.iSubItem = 5;
		break;
	case ROOMNAME:
		lvI.iSubItem = 6;
		break;
	case LASTTEXT:
		lvI.iSubItem = 7;
		break;
	case ACCESS:
		lvI.iSubItem = 8;
		break;
	default:
		return;
	}

	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.iItem = itemRow;
	lvI.pszText = (wchar_t*)arg.c_str();

	SendMessage(listView, LVM_SETITEM, 0, (LPARAM)&lvI);
}

void GuiHelpers::WriteLog(HWND hWnd, wstring text)
{
	HWND edit = GetDlgItem(hWnd, EDIT_INFOBOX);
	int length = GetWindowTextLength(edit);

	text = text + L"\r\n";

	wchar_t* logText = new wchar_t[length + 1];
	wchar_t* combined = new wchar_t[length + text.length() + 1];

	SendMessage(edit, WM_GETTEXT, (WPARAM)length + 1, (LPARAM)logText);
	
	ZeroMemory(combined, length + text.length() + 1);
	lstrcat(combined, logText);
	lstrcat(combined, text.c_str());

	SendMessage(edit, WM_SETTEXT, NULL, (LPARAM)combined);
}