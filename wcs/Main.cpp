#include <Windows.h>
#include <CommCtrl.h>
#include <tchar.h>

#include "resource.h"
#include "GuiHelpers.h"

#pragma comment(linker, "\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	TCHAR szWindowClass[] = _T("wcs2.0.0.0");
	TCHAR szTitle[] = _T("WinMX Chat Server v2.0.0.0-Win32");
	WNDCLASSEX wcex;
	
    wcex.cbSize			= sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wcex))
    {
        MessageBox(NULL,
            _T("Call to RegisterClassEx failed!"),
            szTitle,
            NULL);

        return 1;
    }

	HWND hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		750, 494, // Width, Height
		NULL,
		NULL,
		hInstance,
		NULL
		);

	if (!hWnd)
	{
		MessageBox(NULL,
			_T("Call to CreateWindow failed!"),
			szTitle,
			NULL);

		return 1;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
		{
			// Create Default Font
			HFONT hFont = CreateFont(16, 0, 0, 0, FW_MEDIUM, 0, 0, 0, 0, 0, 0, 0, 0, _T("Arial"));

			// Create Main Menu Bar
			HMENU hMenu = CreateMenu();
			HMENU hSubMenu = CreatePopupMenu();

			AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("File"));
			AppendMenu(hSubMenu, MF_STRING, MENU_CONFIG, _T("Open Configuration File"));
			AppendMenu(hSubMenu, MF_SEPARATOR, NULL, NULL);
			AppendMenu(hSubMenu, MF_STRING, MENU_CONFIG, _T("Exit"));

			hSubMenu = CreatePopupMenu();
			AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("Copy"));
			AppendMenu(hSubMenu, MF_STRING, MENU_COPYCNAME, _T("Channel Name"));
			AppendMenu(hSubMenu, MF_STRING, MENU_COPYTOPIC, _T("Topic"));
			AppendMenu(hSubMenu, MF_STRING, MENU_COPYIP, _T("Host IP Address"));

			hSubMenu = CreatePopupMenu();
			AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("Help"));
			AppendMenu(hSubMenu, MF_STRING, MENU_WEBSITE, _T("Open WCS Website"));

			SetMenu(hWnd, hMenu);
			DeleteObject(hMenu);

			// Create Room Info (Statics)
			HWND StaticUCount = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("User Count: 0"), 
				WS_CHILD | WS_VISIBLE,
				5, 0, 300, 16, 
				hWnd, 
				(HMENU)STATIC_UCOUNT,
				NULL, NULL);

			SendMessage(StaticUCount, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticCName = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("Channel Name: RoomNameHere_000000000000"), 
				WS_CHILD | WS_VISIBLE,
				5, 16, 300, 16, 
				hWnd, 
				(HMENU)STATIC_CNAME,
				NULL, NULL);

			SendMessage(StaticCName, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticTopic = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("Topic: Topic will be here"), 
				WS_CHILD | WS_VISIBLE,
				5, 32, 300, 16, 
				hWnd, 
				(HMENU)STATIC_TOPIC,
				NULL, NULL);

			SendMessage(StaticTopic, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticDLRate = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("Download: 0Kbps (0KB/s)"), 
				WS_CHILD | WS_VISIBLE,
				5, 48, 300, 16, 
				hWnd, 
				(HMENU)STATIC_DLRATE,
				NULL, NULL);

			SendMessage(StaticDLRate, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticULRate = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("Upload: 0Kbps (0KB/s)"), 
				WS_CHILD | WS_VISIBLE,
				5, 64, 300, 16, 
				hWnd, 
				(HMENU)STATIC_ULRATE,
				NULL, NULL);

			SendMessage(StaticULRate, WM_SETFONT, (WPARAM)hFont, TRUE);

			// Primary Information
			HWND StaticPrime1 = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("  Disconnected"), 
				WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_CENTERIMAGE | SS_LEFT,
				335, 4, 199, 24, 
				hWnd, 
				(HMENU)STATIC_PRIME1,
				NULL, NULL);

			SendMessage(StaticPrime1, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticPrime2 = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("  Disconnected"), 
				WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_CENTERIMAGE | SS_LEFT,
				534, 4, 199, 24, 
				hWnd, 
				(HMENU)STATIC_PRIME2,
				NULL, NULL);

			SendMessage(StaticPrime2, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticPrime3 = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("  Disconnected"), 
				WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_CENTERIMAGE | SS_LEFT,
				335, 29, 199, 24, 
				hWnd, 
				(HMENU)STATIC_PRIME3,
				NULL, NULL);

			SendMessage(StaticPrime3, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticPrime4 = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("  Disconnected"), 
				WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_CENTERIMAGE | SS_LEFT,
				534, 29, 199, 24, 
				hWnd, 
				(HMENU)STATIC_PRIME4,
				NULL, NULL);

			SendMessage(StaticPrime4, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticPrime5 = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("  Disconnected"), 
				WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_CENTERIMAGE | SS_LEFT,
				335, 54, 199, 24, 
				hWnd, 
				(HMENU)STATIC_PRIME5,
				NULL, NULL);

			SendMessage(StaticPrime5, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND StaticPrime6 = CreateWindowEx(NULL, 
				_T("STATIC"),
				_T("  Disconnected"), 
				WS_CHILD | WS_VISIBLE | SS_SUNKEN | SS_CENTERIMAGE | SS_LEFT,
				534, 54, 199, 24, 
				hWnd, 
				(HMENU)STATIC_PRIME6,
				NULL, NULL);

			SendMessage(StaticPrime6, WM_SETFONT, (WPARAM)hFont, TRUE);

			// Listbox
			INITCOMMONCONTROLSEX icex;
			icex.dwICC = ICC_LISTVIEW_CLASSES;
			InitCommonControlsEx(&icex);

			HWND UserList = CreateWindow(WC_LISTVIEW,
				_T(""),
				WS_VISIBLE | WS_CHILD | LVS_REPORT | WS_BORDER | WS_HSCROLL | LVS_EDITLABELS,
				1, 80,
				732, 210,
				hWnd, (HMENU)LIST_USERS, NULL, NULL);

			ListView_SetExtendedListViewStyle(UserList, LVS_EX_FULLROWSELECT);
			ListView_SetBkColor(UserList, RGB(0, 0, 0));

			LVCOLUMN lvc;
			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 0;
			lvc.pszText		= L"Name";
			lvc.cx			= 120;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 0, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 1;
			lvc.pszText		= L"Ver";
			lvc.cx			= 75;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 1, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 2;
			lvc.pszText		= L"IP";
			lvc.cx			= 90;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 2, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 3;
			lvc.pszText		= L"Hostname";
			lvc.cx			= 120;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 3, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 4;
			lvc.pszText		= L"Files";
			lvc.cx			= 45;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 4, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 5;
			lvc.pszText		= L"Connection";
			lvc.cx			= 60;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 5, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 6;
			lvc.pszText		= L"Room Name";
			lvc.cx			= 110;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 6, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 7;
			lvc.pszText		= L"Last Text";
			lvc.cx			= 110;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 7, &lvc);

			lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

			lvc.iSubItem	= 8;
			lvc.pszText		= L"Access";
			lvc.cx			= 40;
			lvc.fmt         = LVCFMT_LEFT;

			ListView_InsertColumn(UserList, 8, &lvc);

			// Information Multi-Line Text
			HWND InformationBox = CreateWindowEx(NULL, 
				_T("EDIT"),
				_T(""), 
				WS_CHILD | WS_VISIBLE | WS_BORDER | ES_MULTILINE | ES_READONLY,
				1, 291, 732, 122, 
				hWnd, 
				(HMENU)EDIT_INFOBOX,
				NULL, NULL);

			SendMessage(InformationBox, WM_SETFONT, (WPARAM)hFont, TRUE);

			// Server Text Box
			HWND ServerTextBox = CreateWindowEx(NULL, 
				_T("EDIT"),
				_T(""), 
				WS_CHILD | WS_VISIBLE | WS_BORDER | ES_CENTER,
				1, 414, 532, 20, 
				hWnd, 
				(HMENU)EDIT_INFOBOX,
				NULL, NULL);

			SendMessage(ServerTextBox, WM_SETFONT, (WPARAM)hFont, TRUE);

			// Create Buttons
			HWND ReloadButton = CreateWindowEx(NULL,
				L"BUTTON",
				L"Reload",
				WS_CHILD | WS_VISIBLE,
				533, 413, 100, 22,
				hWnd, 
				(HMENU)BTN_RELOAD,
				NULL, NULL);

			SendMessage(ReloadButton, WM_SETFONT, (WPARAM)hFont, TRUE);

			HWND ExitButton = CreateWindowEx(NULL,
				L"BUTTON",
				L"Exit",
				WS_CHILD | WS_VISIBLE,
				632, 413, 101, 22,
				hWnd, 
				(HMENU)BTN_EXIT,
				NULL, NULL);

			SendMessage(ExitButton, WM_SETFONT, (WPARAM)hFont, TRUE);

		} break;

	case WM_CTLCOLORSTATIC:
		{
			DWORD ControlID = GetDlgCtrlID((HWND)lParam);
			HDC hdcStatic = (HDC)wParam;

			SetBkMode(hdcStatic, TRANSPARENT);

			if (ControlID == STATIC_PRIME1 ||
				ControlID == STATIC_PRIME2 ||
				ControlID == STATIC_PRIME3 ||
				ControlID == STATIC_PRIME4 ||
				ControlID == STATIC_PRIME5 ||
				ControlID == STATIC_PRIME6) 
			{
				wchar_t text[100];
				SendMessage((HWND)lParam, WM_GETTEXT, (WPARAM)100, (LPARAM)&text);

				if (wcsstr(text, L"  Connecting") != NULL || wcsstr(text, L"  Handshake") != NULL)
					SetTextColor(hdcStatic, RGB(255, 255, 0));
				else if (wcsstr(text, L"  Connected") != NULL)
					SetTextColor(hdcStatic, RGB(50, 205, 50));
				else if (wcsstr(text, L"  Disconnected ") != NULL)
					SetTextColor(hdcStatic, RGB(255, 0, 0));
				else
					SetTextColor(hdcStatic, RGB(190, 190, 190));

				return (INT_PTR)(CreateSolidBrush(RGB(0, 0, 0)));
			}

			return (INT_PTR)(HBRUSH)(COLOR_WINDOW);
		}

    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
        break;
    }

    return 0;
}